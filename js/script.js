$(document).ready(function() { 
			$('.menu-item').click(function() {

				var show_menu = $(this).attr("show-menu");
				if(show_menu == "no")
				{
					$('.item').show().css("display","block");
					$(this).attr("show-menu","yes");
				}
				else
				{
					$('.item').hide().css("display","none");
					$(this).attr("show-menu","no");
				}
				event.stopPropagation();
			});
			$(window).not('.item').click(function() {
				var show_menu = $('.menu-item').attr("show-menu");
				if(show_menu == "yes")
				{
					$('.item').hide();
					$('.menu-item').attr("show-menu","no");
				}
			});

    			
	/*************** tooltips  ****************/
	$(".list-services a.tooltips").easyTooltip();
	
}); 